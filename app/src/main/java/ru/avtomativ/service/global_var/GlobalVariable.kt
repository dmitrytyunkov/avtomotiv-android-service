package ru.avtomativ.service.global_var

class GlobalVariable {
    companion object {
        var currCityView: String = "Омск"
        var currCity: String = "omsk"
        const val MIN_BATTERY_CAPACITY = 0
        const val MAX_BATTERY_CAPACITY = 250
        const val MIN_BATTERY_STARTING_CURRENT = 0
        const val MAX_BATTERY_STARTING_CURRENT = 1600
        const val BASE_URL = "https://18.191.211.168/"

        const val FILTER_ACTIVITY = 1
        const val FROM_BATTERY_CAPACITY = "from_battery_capacity"
        const val TO_BATTERY_CAPACITY = "to_battery_capacity"
        const val FROM_BATTERY_STARTING_CURREN = "from_battery_starting_current"
        const val TO_BATTERY_STARTING_CURREN = "to_battery_starting_current"
        const val CURRENT_CITY = "current_city"
        const val CURRENT_CITY_VIEW = "current_city_view"
        const val ID = "id"
        const val TITLE = "title"
    }
}