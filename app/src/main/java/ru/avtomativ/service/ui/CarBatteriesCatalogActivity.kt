package ru.avtomativ.service.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_car_batteries_catalog.*
import ru.avtomativ.service.R
import ru.avtomativ.service.adapter.CatalogAdapter
import ru.avtomativ.service.global_var.GlobalVariable
import ru.avtomativ.service.model.CarBatteryCatalogModel
import ru.avtomativ.service.presenter.CarBatteriesCatalogPresenter
import ru.avtomativ.service.view.CarBatteriesCatalogView

class CarBatteriesCatalogActivity : AppCompatActivity(), CarBatteriesCatalogView {
    private var page: Int = 1
    private var fromCapacity: Int = GlobalVariable.MIN_BATTERY_CAPACITY
    private var toCapacity: Int = GlobalVariable.MAX_BATTERY_CAPACITY
    private var fromStartingCurrent: Int = GlobalVariable.MIN_BATTERY_STARTING_CURRENT
    private var toStartingCurrent: Int = GlobalVariable.MAX_BATTERY_STARTING_CURRENT

    private var carBatteriesCatalog: MutableList<CarBatteryCatalogModel> =
        emptyList<CarBatteryCatalogModel>().toMutableList()
    private var catalogAdapter: CatalogAdapter = CatalogAdapter()

    private var loading: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null) {
            if (savedInstanceState.getString(GlobalVariable.CURRENT_CITY) != null) {
                GlobalVariable.currCity = savedInstanceState.getString(GlobalVariable.CURRENT_CITY)!!
            }
            if (savedInstanceState.getString(GlobalVariable.CURRENT_CITY_VIEW) != null) {
                GlobalVariable.currCityView = savedInstanceState.getString(GlobalVariable.CURRENT_CITY_VIEW)!!
            }
            fromCapacity = savedInstanceState.getInt(GlobalVariable.FROM_BATTERY_CAPACITY)
            toCapacity = savedInstanceState.getInt(GlobalVariable.TO_BATTERY_CAPACITY)
            fromStartingCurrent = savedInstanceState.getInt(GlobalVariable.FROM_BATTERY_STARTING_CURREN)
            toStartingCurrent = savedInstanceState.getInt(GlobalVariable.TO_BATTERY_STARTING_CURREN)
        }

        setContentView(R.layout.activity_car_batteries_catalog)

        // Отображение прогресс бара
        progressBarCtalogActivity.visibility = View.VISIBLE
        // Реализация листенера клика на кнопку Фильтр
        buttonCarBatteriesFilter.setOnClickListener(this::onClickFilter)

        // Задание адаптера для отображения каталога АКБ
        recyclerViewCarBatteriesCatalog.layoutManager = LinearLayoutManager(this)
        recyclerViewCarBatteriesCatalog.adapter = catalogAdapter
        // Задание адаптера для скрола (реализация бесконечной подгрузки)
        recyclerViewCarBatteriesCatalog.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?

                if (!loading && linearLayoutManager!!.itemCount <= linearLayoutManager.findLastVisibleItemPosition() + 5) {
                    loading = true // Флаг загрузки устанавливается
                    page += 1 // Устанавливаем следующую страницу
                    CarBatteriesCatalogPresenter(this@CarBatteriesCatalogActivity).getCarBatteriesCatalog(
                        GlobalVariable.currCity,
                        page,
                        fromCapacity,
                        toCapacity,
                        fromStartingCurrent,
                        toStartingCurrent
                    )
                }
            }
        })
        // Вызов метода презентера для получения каталога авто АКБ от API сервиса
        CarBatteriesCatalogPresenter(this).getCarBatteriesCatalog(
            GlobalVariable.currCity,
            page,
            fromCapacity,
            toCapacity,
            fromStartingCurrent,
            toStartingCurrent
        )
        // Реализация листенера клика на кнопку Повтор
        buttonRetryCatalog.setOnClickListener(this::onClickRetry)
    }

    // Метод обрабатывающий ошибки вернувшиеся после запроса к API сервису
    override fun returnCarBatteriesCatalog(carBatteriesCatalog: List<CarBatteryCatalogModel>) {
        this.carBatteriesCatalog.addAll(carBatteriesCatalog) // Сохранение возвращенного каталога
        progressBarCtalogActivity.visibility = View.GONE // Скрытие прогресс бара
        loading = false // Флаг загрузки снимается
        catalogAdapter.setCatalogItems(carBatteriesCatalog) // Добавление возвращенного списка к отображению
    }

    // Метод обрабатывающий данные вернувшиеся после запроса к API сервису
    override fun returnCarBatteriesCatalogError(error: String) {
        loading = false // Флаг загрузки снимается
        page -= 1
        if (carBatteriesCatalog.isEmpty()) {
            progressBarCtalogActivity.visibility = View.GONE // Скрытие прогресс бара
            // Отображаем сообщение об ошибке
            constraintLayoutErrorConnectionCatalog.visibility = View.VISIBLE
        }
    }

    // Метод реализующий листенер клика на кнопку Фильтр
    private fun onClickFilter(view: View) {
        // Передача фильтру текущих значений
        val intent: Intent = Intent(this, FilterActivity::class.java)
        intent.putExtra(GlobalVariable.FROM_BATTERY_CAPACITY, fromCapacity)
        intent.putExtra(GlobalVariable.TO_BATTERY_CAPACITY, toCapacity)
        intent.putExtra(GlobalVariable.FROM_BATTERY_STARTING_CURREN, fromStartingCurrent)
        intent.putExtra(GlobalVariable.TO_BATTERY_STARTING_CURREN, toStartingCurrent)
        startActivityForResult(intent, GlobalVariable.FILTER_ACTIVITY)
    }

    // Метод реализующий листенер клика на кнопку Фильтр (Тулбар)
    private fun onClickFilter() {
        // Передача фильтру текущих значений
        val intent: Intent = Intent(this, FilterActivity::class.java)
        intent.putExtra(GlobalVariable.FROM_BATTERY_CAPACITY, fromCapacity)
        intent.putExtra(GlobalVariable.TO_BATTERY_CAPACITY, toCapacity)
        intent.putExtra(GlobalVariable.FROM_BATTERY_STARTING_CURREN, fromStartingCurrent)
        intent.putExtra(GlobalVariable.TO_BATTERY_STARTING_CURREN, toStartingCurrent)
        startActivityForResult(intent, GlobalVariable.FILTER_ACTIVITY)
    }

    // Метод обрабатывающий результат полученный от фильра
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GlobalVariable.FILTER_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    fromCapacity =
                        data.getIntExtra(GlobalVariable.FROM_BATTERY_CAPACITY, GlobalVariable.MIN_BATTERY_CAPACITY)
                    toCapacity =
                        data.getIntExtra(GlobalVariable.TO_BATTERY_CAPACITY, GlobalVariable.MAX_BATTERY_CAPACITY)
                    fromStartingCurrent = data.getIntExtra(
                        GlobalVariable.FROM_BATTERY_STARTING_CURREN,
                        GlobalVariable.MIN_BATTERY_STARTING_CURRENT
                    )
                    toStartingCurrent = data.getIntExtra(
                        GlobalVariable.TO_BATTERY_STARTING_CURREN,
                        GlobalVariable.MAX_BATTERY_STARTING_CURRENT
                    )
                    page = 1
                    carBatteriesCatalog.clear()
                    catalogAdapter.clearCatalogItems()
                    progressBarCtalogActivity.visibility = View.VISIBLE
                    CarBatteriesCatalogPresenter(this).getCarBatteriesCatalog(
                        GlobalVariable.currCity,
                        page,
                        fromCapacity,
                        toCapacity,
                        fromStartingCurrent,
                        toStartingCurrent
                    )
                }
            }
        }
    }

    // Метод реализующий листенер клика на кнопку Повтор
    private fun onClickRetry(view: View) {
        // Скрытие сообщения об ошибке
        constraintLayoutErrorConnectionCatalog.visibility = View.GONE
        // Отображение прогресс бара
        progressBarCtalogActivity.visibility = View.VISIBLE
        // Вызов метода презентера для получения каталога авто АКБ от API сервиса
        CarBatteriesCatalogPresenter(this).getCarBatteriesCatalog(
            GlobalVariable.currCity,
            page,
            fromCapacity,
            toCapacity,
            fromStartingCurrent,
            toStartingCurrent
        )
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putString(GlobalVariable.CURRENT_CITY, GlobalVariable.currCity)
            putString(GlobalVariable.CURRENT_CITY_VIEW, GlobalVariable.currCityView)
            putInt(GlobalVariable.FROM_BATTERY_CAPACITY, fromCapacity)
            putInt(GlobalVariable.TO_BATTERY_CAPACITY, toCapacity)
            putInt(GlobalVariable.FROM_BATTERY_STARTING_CURREN, fromStartingCurrent)
            putInt(GlobalVariable.TO_BATTERY_STARTING_CURREN, toStartingCurrent)
        }
        super.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.catalog_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.actionFilter) {
            onClickFilter()
        }
        return super.onOptionsItemSelected(item)
    }
}
