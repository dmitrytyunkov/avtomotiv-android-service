package ru.avtomativ.service.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.AdapterView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_select_city.*
import ru.avtomativ.service.R
import ru.avtomativ.service.adapter.CitiesAdapter
import ru.avtomativ.service.global_var.GlobalVariable
import ru.avtomativ.service.model.CityModel
import ru.avtomativ.service.presenter.SelectCityPresenter
import ru.avtomativ.service.view.SelectCityView

class SelectCityActivity : AppCompatActivity(), SelectCityView {
    private var cities: List<CityModel> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null) {
            if (savedInstanceState.getString(GlobalVariable.CURRENT_CITY) != null) {
                GlobalVariable.currCity = savedInstanceState.getString(GlobalVariable.CURRENT_CITY)!!
            }
            if (savedInstanceState.getString(GlobalVariable.CURRENT_CITY_VIEW) != null) {
                GlobalVariable.currCityView = savedInstanceState.getString(GlobalVariable.CURRENT_CITY_VIEW)!!
            }
        }

        setContentView(R.layout.activity_select_city)
        supportActionBar?.hide() // Скрытие тулбара
        // Отображение логотипа
        Picasso.with(this).load(R.drawable.logo_main_png).into(imageViewLogo)
        // Отображение прогресс бара
        progressBarSelectCityActivity.visibility = View.VISIBLE
        getLocation() // Вызов метода для определения города по геолокации
        // Вызов метода презентера для получения списка городов от API сервиса
        SelectCityPresenter(this).getCities()
        // Реализация листенера выбора города из выпадающего списка
        spinnerSelectCity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if (cities.isNotEmpty()) {
                    GlobalVariable.currCity =
                        cities[p3.toInt()].city // Сохранение кода текущего выбранного города для выполнения запросов
                    GlobalVariable.currCityView =
                        cities[p3.toInt()].cityView // Сохранение отображения текущего выбранного города
                    refreshCurrCityView()
                } else {
                    Log.d("SPINNER ERROR", "No elements in spinner")
                }
            }
        }
        // Реализация листенера клика на кнопку ОК
        buttonOK.setOnClickListener(this::onClickOK)
        // Реализация листенера клика на кнопку Повтор
        buttonRetrySelectCity.setOnClickListener(this::onClickRetry)
    }

    // Заглушка для определения текущего города по геолокации
    private fun getLocation() {
        GlobalVariable.currCity = "novosibirsk" // Сохранение кода текущего города для выполнения запросов
        GlobalVariable.currCityView = "Новосибирск" // Сохранение отображения текущего города
        refreshCurrCityView() // Обновление текста текущего выбранного города
    }

    // Метод для обновления текста текущего города
    private fun refreshCurrCityView() {
        textViewYourCity.text = "%s %s?".format(getString(R.string.your_city), GlobalVariable.currCityView)
    }

    // Метод обрабатывающий данные вернувшиеся после запроса к API сервису
    override fun returnCities(cities: List<CityModel>) {
        // Скрытие прогресс бара
        progressBarSelectCityActivity.visibility = View.GONE
        // Отображение элементов активити
        textViewYourCity.visibility = View.VISIBLE
        constraintLayoutSelectCity.visibility = View.VISIBLE
        this.cities = cities // Сохранение списка возвращенных городов
        // Получение адаптера выпадающего списка городов
        spinnerSelectCity.adapter = CitiesAdapter.getAdapter(this, cities)
        // Выбор в списке городов текущего (определенного геолокацией)
        spinnerSelectCity.setSelection(cities.indexOf(CityModel(GlobalVariable.currCity, GlobalVariable.currCityView)))
    }

    // Метод обрабатывающий ошибки вернувшиеся после запроса к API сервису
    override fun returnCitiesError(error: String) {
        // Скрытие прогрес бара
        progressBarSelectCityActivity.visibility = View.GONE
        // Отображение сообщения об ошибке
        constraintLayoutErrorConnectionSelectCity.visibility = View.VISIBLE
    }

    // Метод реализующий листенер клика на кнопку ОК
    private fun onClickOK(view: View) {
        val intent = Intent(this, MainMenuActivity::class.java)
        startActivity(intent)
    }

    // Метод реализующий листенер клика на кнопку Повтор
    private fun onClickRetry(view: View) {
        // Скрытие сообщения об ошибке
        constraintLayoutErrorConnectionSelectCity.visibility = View.GONE
        // Отображение прогресс бара
        progressBarSelectCityActivity.visibility = View.VISIBLE
        // Вызов метода презентера для получения списка городов от API сервиса
        SelectCityPresenter(this).getCities()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putString(GlobalVariable.CURRENT_CITY, GlobalVariable.currCity)
            putString(GlobalVariable.CURRENT_CITY_VIEW, GlobalVariable.currCityView)
        }
        super.onSaveInstanceState(outState)
    }
}
