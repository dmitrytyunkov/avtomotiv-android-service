package ru.avtomativ.service.model


import com.google.gson.annotations.SerializedName

data class Shop(
    @SerializedName("shop_status")
    val shopStatus: String,
    @SerializedName("shop_title")
    val shopAddress: String,
    @SerializedName("shop_phone")
    val shopPhone: String
)