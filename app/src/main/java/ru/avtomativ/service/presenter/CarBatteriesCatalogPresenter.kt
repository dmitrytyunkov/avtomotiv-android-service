package ru.avtomativ.service.presenter

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.avtomativ.service.global_var.GlobalVariable
import ru.avtomativ.service.model.CarBatteryCatalogModel
import ru.avtomativ.service.network.AvtomotivService
import ru.avtomativ.service.view.CarBatteriesCatalogView

class CarBatteriesCatalogPresenter(private val carBatteriesCatalogView: CarBatteriesCatalogView) {
    fun getCarBatteriesCatalog(
        city: String,
        page: Int,
        fromCapacity: Int,
        toCapacity: Int,
        fromStartingCurrent: Int,
        toStartingCurrent: Int
    ) {
        val avtomotivService = AvtomotivService.create(GlobalVariable.BASE_URL) // Создаем подключение к API сервису
        avtomotivService.getCarBatteriesCatalog(
            city,
            page,
            fromCapacity,
            toCapacity,
            fromStartingCurrent,
            toStartingCurrent
        ).enqueue(object : Callback<List<CarBatteryCatalogModel>> {
            override fun onFailure(call: Call<List<CarBatteryCatalogModel>>, t: Throwable) {
                Log.d("REQUEST ERROR", t.message)
                carBatteriesCatalogView.returnCarBatteriesCatalogError(t.message!!)
            }

            override fun onResponse(
                call: Call<List<CarBatteryCatalogModel>>,
                response: Response<List<CarBatteryCatalogModel>>
            ) {
                if (response.code() != 200) {
                    Log.d("REQUEST ERROR", response.code().toString() + ' '.toString() + response.message())
                    carBatteriesCatalogView.returnCarBatteriesCatalogError(response.code().toString() + ' '.toString() + response.message())
                } else {
                    Log.d("REQUEST OK", response.body()!!.toString())
                    carBatteriesCatalogView.returnCarBatteriesCatalog(response.body()!!)
                }
            }
        })
    }
}