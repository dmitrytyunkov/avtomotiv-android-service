package ru.avtomativ.service.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_filter.*
import ru.avtomativ.service.R
import ru.avtomativ.service.global_var.GlobalVariable

class FilterActivity : AppCompatActivity() {
    var fromCapacity: Int = 0
    var toCapacity: Int = 0
    var fromStartingCurrent: Int = 0
    var toStartingCurrent: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)

        if (savedInstanceState != null) {
            if (savedInstanceState.getString(GlobalVariable.CURRENT_CITY) != null) {
                GlobalVariable.currCity = savedInstanceState.getString(GlobalVariable.CURRENT_CITY)!!
            }
            if (savedInstanceState.getString(GlobalVariable.CURRENT_CITY_VIEW) != null) {
                GlobalVariable.currCityView = savedInstanceState.getString(GlobalVariable.CURRENT_CITY_VIEW)!!
            }
            fromCapacity = savedInstanceState.getInt(GlobalVariable.FROM_BATTERY_CAPACITY)
            toCapacity = savedInstanceState.getInt(GlobalVariable.TO_BATTERY_CAPACITY)
            fromStartingCurrent = savedInstanceState.getInt(GlobalVariable.FROM_BATTERY_STARTING_CURREN)
            toStartingCurrent = savedInstanceState.getInt(GlobalVariable.TO_BATTERY_STARTING_CURREN)
        }

        if (intent.extras != null) {
            // Получение значений текущих фильтров
            fromCapacity = intent.getIntExtra(GlobalVariable.FROM_BATTERY_CAPACITY, GlobalVariable.MIN_BATTERY_CAPACITY)
            toCapacity = intent.getIntExtra(GlobalVariable.TO_BATTERY_CAPACITY, GlobalVariable.MAX_BATTERY_CAPACITY)
            fromStartingCurrent = intent.getIntExtra(
                GlobalVariable.FROM_BATTERY_STARTING_CURREN,
                GlobalVariable.MIN_BATTERY_STARTING_CURRENT
            )
            toStartingCurrent = intent.getIntExtra(
                GlobalVariable.TO_BATTERY_STARTING_CURREN,
                GlobalVariable.MAX_BATTERY_STARTING_CURRENT
            )
        }

        rangeSeekbarCapacity.setMinValue(GlobalVariable.MIN_BATTERY_CAPACITY.toFloat())
        rangeSeekbarCapacity.setMaxValue(GlobalVariable.MAX_BATTERY_CAPACITY.toFloat())
        rangeSeekbarStartigCurrent.setMinValue(GlobalVariable.MIN_BATTERY_STARTING_CURRENT.toFloat())
        rangeSeekbarStartigCurrent.setMaxValue(GlobalVariable.MAX_BATTERY_STARTING_CURRENT.toFloat())

        // Задание листенера изменения текста поля с начальной емкостью
        editTextFromCapacity.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.isNullOrEmpty()) {
                    if (!editTextToCapacity.text.isNullOrEmpty() && p0.toString().toInt() >= editTextToCapacity.text.toString().toInt()) {
                        editTextFromCapacity.setText((editTextToCapacity.text.toString().toInt() - 1).toString())
                    } else if (p0.toString().toInt() < GlobalVariable.MIN_BATTERY_CAPACITY) {
                        editTextFromCapacity.setText(GlobalVariable.MIN_BATTERY_CAPACITY.toString())
                    } else if (!editTextToCapacity.text.isNullOrEmpty() && p0.toString().toInt() != rangeSeekbarCapacity.selectedMinValue.toInt()) {
                        rangeSeekbarCapacity.setMinStartValue(p0.toString().toFloat())
                            .setMaxStartValue(rangeSeekbarCapacity.selectedMaxValue.toFloat()).apply()
                    }
                    fromCapacity = p0.toString().toInt()
                }
                Log.d("TEXT_CHANGE", "fromCapacity %s".format(p0.toString()))
            }
        })
        // Задание листенера изменения текста поля с конечной емкостью
        editTextToCapacity.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.isNullOrEmpty()) {
                    if (!editTextFromCapacity.text.isNullOrEmpty() && p0.toString().toInt() <= editTextFromCapacity.text.toString().toInt()) {
                        editTextToCapacity.setText((editTextFromCapacity.text.toString().toInt() + 1).toString())
                    } else if (p0.toString().toInt() > GlobalVariable.MAX_BATTERY_CAPACITY) {
                        editTextToCapacity.setText(GlobalVariable.MAX_BATTERY_CAPACITY.toString())
                    } else if (!editTextFromCapacity.text.isNullOrEmpty() && p0.toString().toInt() != rangeSeekbarCapacity.selectedMaxValue.toInt()) {
                        rangeSeekbarCapacity.setMinStartValue(rangeSeekbarCapacity.selectedMinValue.toFloat())
                            .setMaxStartValue(p0.toString().toFloat()).apply()
                    }
                    toCapacity = p0.toString().toInt()
                }
                Log.d("TEXT_CHANGE", "toCapacity %s".format(p0.toString()))
            }
        })
        // Задание листенера изменения текста поля с начальным стартовым током
        editTextFromStartigCurrent.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.isNullOrEmpty()) {
                    if (!editTextToStartigCurrent.text.isNullOrEmpty() && p0.toString().toInt() >= editTextToStartigCurrent.text.toString().toInt()) {
                        editTextFromStartigCurrent.setText((editTextToStartigCurrent.text.toString().toInt() - 1).toString())
                    } else if (p0.toString().toInt() < GlobalVariable.MIN_BATTERY_STARTING_CURRENT) {
                        editTextFromStartigCurrent.setText(GlobalVariable.MIN_BATTERY_STARTING_CURRENT.toString())
                    } else if (!editTextToStartigCurrent.text.isNullOrEmpty() && p0.toString().toInt() != rangeSeekbarStartigCurrent.selectedMinValue.toInt()) {
                        rangeSeekbarStartigCurrent.setMinStartValue(p0.toString().toFloat())
                            .setMaxStartValue(rangeSeekbarStartigCurrent.selectedMaxValue.toFloat()).apply()
                    }
                    fromStartingCurrent = p0.toString().toInt()
                }
                Log.d("TEXT_CHANGE", "fromStartingCurrent %s".format(p0.toString()))
            }
        })
        // Задание листенера изменения текста поля с конечным стартовым током
        editTextToStartigCurrent.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.isNullOrEmpty()) {
                    if (!editTextFromStartigCurrent.text.isNullOrEmpty() && p0.toString().toInt() <= editTextFromStartigCurrent.text.toString().toInt()) {
                        editTextToStartigCurrent.setText((editTextFromStartigCurrent.text.toString().toInt() + 1).toString())
                    } else if (p0.toString().toInt() > GlobalVariable.MAX_BATTERY_STARTING_CURRENT) {
                        editTextToStartigCurrent.setText(GlobalVariable.MAX_BATTERY_STARTING_CURRENT.toString())
                    } else if (!editTextFromStartigCurrent.text.isNullOrEmpty() && p0.toString().toInt() != rangeSeekbarStartigCurrent.selectedMaxValue.toInt()) {
                        rangeSeekbarStartigCurrent.setMinStartValue(rangeSeekbarStartigCurrent.selectedMinValue.toFloat())
                            .setMaxStartValue(p0.toString().toFloat()).apply()
                    }
                    toStartingCurrent = p0.toString().toInt()
                }
                Log.d("TEXT_CHANGE", "toStartingCurrent %s".format(p0.toString()))
            }
        })
        // Задание текущих фильтров
        rangeSeekbarCapacity.setMinStartValue(fromCapacity.toFloat()).setMaxStartValue(toCapacity.toFloat()).apply()
        rangeSeekbarStartigCurrent.setMinStartValue(fromStartingCurrent.toFloat())
            .setMaxStartValue(toStartingCurrent.toFloat()).apply()
        // Задание листенера toggleButton'а для емкости
        toggleButtonCapacity.setOnCheckedChangeListener { buttonView, isChecked ->
            constraintLayoutCapacity.visibility = if (isChecked) View.VISIBLE else View.GONE
        }
        // Задание листенера toggleButton'а для стартового тока
        toggleButtonStartingCurrent.setOnCheckedChangeListener { buttonView, isChecked ->
            constraintLayoutStartingCurrent.visibility = if (isChecked) View.VISIBLE else View.GONE
        }
        // Задание листенера seekbar'а для емкости
        rangeSeekbarCapacity.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            if (editTextFromCapacity.text.isNullOrEmpty() || minValue.toInt() != editTextFromCapacity.text.toString().toInt()) {
                Log.d("TEXT_!!!", "min: %s, edMin: %s".format(minValue, editTextFromCapacity.text))
                editTextFromCapacity.setText(minValue.toString())
            }
            if (editTextToCapacity.text.isNullOrEmpty() || maxValue.toInt() != editTextToCapacity.text.toString().toInt()) {
                editTextToCapacity.setText(maxValue.toString())
                Log.d("TEXT_!", "!!!!!")
            }
        }
        // Задание листенера seekbar'а для стартового тока
        rangeSeekbarStartigCurrent.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            if (editTextFromStartigCurrent.text.isNullOrEmpty() || minValue.toInt() != editTextFromStartigCurrent.text.toString().toInt()) {
                editTextFromStartigCurrent.setText(minValue.toString())
            }
            if (editTextToStartigCurrent.text.isNullOrEmpty() || maxValue.toInt() != editTextToStartigCurrent.text.toString().toInt()) {
                editTextToStartigCurrent.setText(maxValue.toString())
            }
        }
        // Реализация листенера клика на кнопку Сброс
        buttonReset.setOnClickListener(this::onClickReset)
        // Реализация листенера клика на кнопку Показать
        buttonShow.setOnClickListener(this::onClickShow)
    }

    // Метод реализующий листенер клика на кнопку Сброс
    private fun onClickReset(view: View) {
        editTextFromCapacity.setText(GlobalVariable.MIN_BATTERY_CAPACITY.toString())
        editTextToCapacity.setText(GlobalVariable.MAX_BATTERY_CAPACITY.toString())
        editTextFromStartigCurrent.setText(GlobalVariable.MIN_BATTERY_STARTING_CURRENT.toString())
        editTextToStartigCurrent.setText(GlobalVariable.MAX_BATTERY_STARTING_CURRENT.toString())
    }

    // Метод реализующий листенер клика на кнопку Показать
    private fun onClickShow(view: View) {
        val intent: Intent = Intent()
        intent.putExtra(GlobalVariable.FROM_BATTERY_CAPACITY, fromCapacity)
        intent.putExtra(GlobalVariable.TO_BATTERY_CAPACITY, toCapacity)
        intent.putExtra(GlobalVariable.FROM_BATTERY_STARTING_CURREN, fromStartingCurrent)
        intent.putExtra(GlobalVariable.TO_BATTERY_STARTING_CURREN, toStartingCurrent)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putString(GlobalVariable.CURRENT_CITY, GlobalVariable.currCity)
            putString(GlobalVariable.CURRENT_CITY_VIEW, GlobalVariable.currCityView)
        }
        super.onSaveInstanceState(outState)
    }
}
