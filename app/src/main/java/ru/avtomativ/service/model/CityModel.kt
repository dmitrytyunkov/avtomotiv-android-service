package ru.avtomativ.service.model


import com.google.gson.annotations.SerializedName

data class CityModel(
    @SerializedName("city")
    val city: String,
    @SerializedName("cityView")
    val cityView: String
) {
    override fun toString(): String {
        return cityView
    }
}