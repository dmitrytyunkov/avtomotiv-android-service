package ru.avtomativ.service.view

import ru.avtomativ.service.model.CarBatteryDetailsModel

interface CarBatteryDetailsView {
    // Метод обрабатывающий данные вернувшиеся после запроса к API сервису
    fun returnCarBatteryDetails(carBatteryDetails: CarBatteryDetailsModel)

    // Метод обрабатывающий ошибки вернувшиеся после запроса к API сервису
    fun returnCarBatteryDetailsError(error: String)
}