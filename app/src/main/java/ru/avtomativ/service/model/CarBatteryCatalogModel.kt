package ru.avtomativ.service.model


import com.google.gson.annotations.SerializedName

data class CarBatteryCatalogModel(
    @SerializedName("capacity")
    val capacity: String,
    @SerializedName("dimensions")
    val dimensions: String,
    @SerializedName("exchange_price")
    val exchangePrice: String,
    @SerializedName("goods_sale")
    val goodsSale: Boolean,
    @SerializedName("id")
    val id: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("polarity")
    val polarity: String,
    @SerializedName("price")
    val price: String,
    @SerializedName("recommend")
    val recommend: Boolean,
    @SerializedName("starting_current")
    val startingCurrent: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("warranty")
    val warranty: String
)