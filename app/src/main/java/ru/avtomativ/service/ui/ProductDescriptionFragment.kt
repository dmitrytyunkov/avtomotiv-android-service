package ru.avtomativ.service.ui


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.avtomativ.service.R
import ru.avtomativ.service.model.CarBatteryDetailsModel

private const val ARG_BATTERY = "battery"

/**
 * A simple [Fragment] subclass.
 *
 */
class ProductDescriptionFragment : Fragment() {
    companion object {
        var battery: CarBatteryDetailsModel? = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_product_description, container, false)
        // одумать более оптимальный код
        val textViewStatusTitle: TextView = view.findViewById(R.id.textViewStatusTitle)
        val textViewStatusValue: TextView = view.findViewById(R.id.textViewStatusValue)
        val textViewClassTitle: TextView = view.findViewById(R.id.textViewClassTitle)
        val textViewClassValue: TextView = view.findViewById(R.id.textViewClassValue)
        val textViewAGMTitle: TextView = view.findViewById(R.id.textViewAGMTitle)
        val textViewAGMValue: TextView = view.findViewById(R.id.textViewAGMValue)
        val textViewEFBTitle: TextView = view.findViewById(R.id.textViewEFBTitle)
        val textViewEFBValue: TextView = view.findViewById(R.id.textViewEFBValue)
        val textViewPunchFrameTitle: TextView = view.findViewById(R.id.textViewPunchFrameTitle)
        val textViewPunchFrameValue: TextView = view.findViewById(R.id.textViewPunchFrameValue)
        val textViewAgTitle: TextView = view.findViewById(R.id.textViewAgTitle)
        val textViewAgValue: TextView = view.findViewById(R.id.textViewAgValue)
        val textViewVoltageTitle: TextView = view.findViewById(R.id.textViewVoltageTitle)
        val textViewVoltageValue: TextView = view.findViewById(R.id.textViewVoltageValue)
        val textViewTypeTitle: TextView = view.findViewById(R.id.textViewTypeTitle)
        val textViewTypeValue: TextView = view.findViewById(R.id.textViewTypeValue)
        val textViewStandardSizeTitle: TextView = view.findViewById(R.id.textViewStandardSizeTitle)
        val textViewStandardSizeValue: TextView = view.findViewById(R.id.textViewStandardSizeValue)
        val textViewLengthTitle: TextView = view.findViewById(R.id.textViewLengthTitle)
        val textViewLengthValue: TextView = view.findViewById(R.id.textViewLengthValue)
        val textViewWidthTitle: TextView = view.findViewById(R.id.textViewWidthTitle)
        val textViewWidthValue: TextView = view.findViewById(R.id.textViewWidthValue)
        val textViewHeightTitle: TextView = view.findViewById(R.id.textViewHeightTitle)
        val textViewHeightValue: TextView = view.findViewById(R.id.textViewHeightValue)
        val textViewPolarityTitle: TextView = view.findViewById(R.id.textViewPolarityTitle)
        val textViewPolarityValue: TextView = view.findViewById(R.id.textViewPolarityValue)
        val textViewTerminalTypeTitle: TextView = view.findViewById(R.id.textViewTerminalTypeTitle)
        val textViewTerminalTypeValue: TextView = view.findViewById(R.id.textViewTerminalTypeValue)
        val textViewBottomMountTitle: TextView = view.findViewById(R.id.textViewBottomMountTitle)
        val textViewBottomMountValue: TextView = view.findViewById(R.id.textViewBottomMountValue)
        val textViewServiceabilityTitle: TextView = view.findViewById(R.id.textViewServiceabilityTitle)
        val textViewServiceabilityValue: TextView = view.findViewById(R.id.textViewServiceabilityValue)
        val textViewWarrantyTitle: TextView = view.findViewById(R.id.textViewWarrantyTitle)
        val textViewWarrantyValue: TextView = view.findViewById(R.id.textViewWarrantyValue)
        val textViewBrandTitle: TextView = view.findViewById(R.id.textViewBrandTitle)
        val textViewBrandValue: TextView = view.findViewById(R.id.textViewBrandValue)
        val textViewManufacturerTitle: TextView = view.findViewById(R.id.textViewManufacturerTitle)
        val textViewManufacturerValue: TextView = view.findViewById(R.id.textViewManufacturerValue)
        val textViewProductionRegionTitle: TextView = view.findViewById(R.id.textViewProductionRegionTitle)
        val textViewProductionRegionValue: TextView = view.findViewById(R.id.textViewProductionRegionValue)
        val textViewCapacityTitle: TextView = view.findViewById(R.id.textViewCapacityTitle)
        val textViewCapacityValue: TextView = view.findViewById(R.id.textViewCapacityValue)
        val textViewStartingCurrentTitle: TextView = view.findViewById(R.id.textViewStartingCurrentTitle)
        val textViewStartingCurrentValue: TextView = view.findViewById(R.id.textViewStartingCurrentValue)
        val textViewProductCodeTitle: TextView = view.findViewById(R.id.textViewProductCodeTitle)
        val textViewProductCodeValue: TextView = view.findViewById(R.id.textViewProductCodeValue)

        if (battery != null) {

            if (battery!!.status.isNotEmpty()) {
                textViewStatusTitle.visibility = View.VISIBLE
                textViewStatusValue.visibility = View.VISIBLE
                textViewStatusValue.text = battery!!.status
            } else {
                textViewStatusTitle.visibility = View.GONE
                textViewStatusValue.visibility = View.GONE
                textViewStatusValue.text = ""
            }
            if (battery!!.classBattery.isNotEmpty()) {
                textViewClassTitle.visibility = View.VISIBLE
                textViewClassValue.visibility = View.VISIBLE
                textViewClassValue.text = battery!!.classBattery
            } else {
                textViewClassTitle.visibility = View.GONE
                textViewClassValue.visibility = View.GONE
                textViewClassValue.text = ""
            }
            if (battery!!.agm.isNotEmpty()) {
                textViewAGMTitle.visibility = View.VISIBLE
                textViewAGMValue.visibility = View.VISIBLE
                textViewAGMValue.text = battery!!.agm
            } else {
                textViewAGMTitle.visibility = View.GONE
                textViewAGMValue.visibility = View.GONE
                textViewAGMValue.text = ""
            }
            if (battery!!.efb.isNotEmpty()) {
                textViewEFBTitle.visibility = View.VISIBLE
                textViewEFBValue.visibility = View.VISIBLE
                textViewEFBValue.text = battery!!.efb
            } else {
                textViewEFBTitle.visibility = View.GONE
                textViewEFBValue.visibility = View.GONE
                textViewEFBValue.text = ""
            }
            if (battery!!.punchFrame.isNotEmpty()) {
                textViewPunchFrameTitle.visibility = View.VISIBLE
                textViewPunchFrameValue.visibility = View.VISIBLE
                textViewPunchFrameValue.text = battery!!.punchFrame
            } else {
                textViewPunchFrameTitle.visibility = View.GONE
                textViewPunchFrameValue.visibility = View.GONE
                textViewPunchFrameValue.text = ""
            }
            if (battery!!.ag.isNotEmpty()) {
                textViewAgTitle.visibility = View.VISIBLE
                textViewAgValue.visibility = View.VISIBLE
                textViewAgValue.text = battery!!.ag
            } else {
                textViewAgTitle.visibility = View.GONE
                textViewAgValue.visibility = View.GONE
                textViewAgValue.text = ""
            }
            if (battery!!.voltage.isNotEmpty()) {
                textViewVoltageTitle.visibility = View.VISIBLE
                textViewVoltageValue.visibility = View.VISIBLE
                textViewVoltageValue.text = battery!!.voltage
            } else {
                textViewVoltageTitle.visibility = View.GONE
                textViewVoltageValue.visibility = View.GONE
                textViewVoltageValue.text = ""
            }
            if (battery!!.type.isNotEmpty()) {
                textViewTypeTitle.visibility = View.VISIBLE
                textViewTypeValue.visibility = View.VISIBLE
                textViewTypeValue.text = battery!!.type
            } else {
                textViewTypeTitle.visibility = View.GONE
                textViewTypeValue.visibility = View.GONE
                textViewTypeValue.text = ""
            }
            if (battery!!.standardSize.isNotEmpty()) {
                textViewStandardSizeTitle.visibility = View.VISIBLE
                textViewStandardSizeValue.visibility = View.VISIBLE
                textViewStandardSizeValue.text = battery!!.standardSize
            } else {
                textViewStandardSizeTitle.visibility = View.GONE
                textViewStandardSizeValue.visibility = View.GONE
                textViewStandardSizeValue.text = ""
            }
            if (battery!!.length.isNotEmpty()) {
                textViewLengthTitle.visibility = View.VISIBLE
                textViewLengthValue.visibility = View.VISIBLE
                textViewLengthValue.text = battery!!.length
            } else {
                textViewLengthTitle.visibility = View.GONE
                textViewLengthValue.visibility = View.GONE
                textViewLengthValue.text = ""
            }
            if (battery!!.width.isNotEmpty()) {
                textViewWidthTitle.visibility = View.VISIBLE
                textViewWidthValue.visibility = View.VISIBLE
                textViewWidthValue.text = battery!!.width
            } else {
                textViewWidthTitle.visibility = View.GONE
                textViewWidthValue.visibility = View.GONE
                textViewWidthValue.text = ""
            }
            if (battery!!.height.isNotEmpty()) {
                textViewHeightTitle.visibility = View.VISIBLE
                textViewHeightValue.visibility = View.VISIBLE
                textViewHeightValue.text = battery!!.height
            } else {
                textViewHeightTitle.visibility = View.GONE
                textViewHeightValue.visibility = View.GONE
                textViewHeightValue.text = ""
            }
            if (battery!!.polarity.isNotEmpty()) {
                textViewPolarityTitle.visibility = View.VISIBLE
                textViewPolarityValue.visibility = View.VISIBLE
                textViewPolarityValue.text = battery!!.polarity
            } else {
                textViewPolarityTitle.visibility = View.GONE
                textViewPolarityValue.visibility = View.GONE
                textViewPolarityValue.text = ""
            }
            if (battery!!.terminalType.isNotEmpty()) {
                textViewTerminalTypeTitle.visibility = View.VISIBLE
                textViewTerminalTypeValue.visibility = View.VISIBLE
                textViewTerminalTypeValue.text = battery!!.terminalType
            } else {
                textViewTerminalTypeTitle.visibility = View.GONE
                textViewTerminalTypeValue.visibility = View.GONE
                textViewTerminalTypeValue.text = ""
            }
            if (battery!!.bottomMount.isNotEmpty()) {
                textViewBottomMountTitle.visibility = View.VISIBLE
                textViewBottomMountValue.visibility = View.VISIBLE
                textViewBottomMountValue.text = battery!!.bottomMount
            } else {
                textViewBottomMountTitle.visibility = View.GONE
                textViewBottomMountValue.visibility = View.GONE
                textViewBottomMountValue.text = ""
            }
            if (battery!!.serviceability.isNotEmpty()) {
                textViewServiceabilityTitle.visibility = View.VISIBLE
                textViewServiceabilityValue.visibility = View.VISIBLE
                textViewServiceabilityValue.text = battery!!.serviceability
            } else {
                textViewServiceabilityTitle.visibility = View.GONE
                textViewServiceabilityValue.visibility = View.GONE
                textViewServiceabilityValue.text = ""
            }
            if (battery!!.warranty.isNotEmpty()) {
                textViewWarrantyTitle.visibility = View.VISIBLE
                textViewWarrantyValue.visibility = View.VISIBLE
                textViewWarrantyValue.text = battery!!.warranty
            } else {
                textViewWarrantyTitle.visibility = View.GONE
                textViewWarrantyValue.visibility = View.GONE
                textViewWarrantyValue.text = ""
            }
            if (battery!!.brand.isNotEmpty()) {
                textViewBrandTitle.visibility = View.VISIBLE
                textViewBrandValue.visibility = View.VISIBLE
                textViewBrandValue.text = battery!!.brand
            } else {
                textViewBrandTitle.visibility = View.GONE
                textViewBrandValue.visibility = View.GONE
                textViewBrandValue.text = ""
            }
            if (battery!!.manufacturer.isNotEmpty()) {
                textViewManufacturerTitle.visibility = View.VISIBLE
                textViewManufacturerValue.visibility = View.VISIBLE
                textViewManufacturerValue.text = battery!!.manufacturer
            } else {
                textViewManufacturerTitle.visibility = View.GONE
                textViewManufacturerValue.visibility = View.GONE
                textViewManufacturerValue.text = ""
            }
            if (battery!!.productionRegion.isNotEmpty()) {
                textViewProductionRegionTitle.visibility = View.VISIBLE
                textViewProductionRegionValue.visibility = View.VISIBLE
                textViewProductionRegionValue.text = battery!!.productionRegion
            } else {
                textViewProductionRegionTitle.visibility = View.GONE
                textViewProductionRegionValue.visibility = View.GONE
                textViewProductionRegionValue.text = ""
            }
            if (battery!!.capacity.isNotEmpty()) {
                textViewCapacityTitle.visibility = View.VISIBLE
                textViewCapacityValue.visibility = View.VISIBLE
                textViewCapacityValue.text = battery!!.capacity
            } else {
                textViewCapacityTitle.visibility = View.GONE
                textViewCapacityValue.visibility = View.GONE
                textViewCapacityValue.text = ""
            }
            if (battery!!.startingCurrent.isNotEmpty()) {
                textViewStartingCurrentTitle.visibility = View.VISIBLE
                textViewStartingCurrentValue.visibility = View.VISIBLE
                textViewStartingCurrentValue.text = battery!!.startingCurrent
            } else {
                textViewStartingCurrentTitle.visibility = View.GONE
                textViewStartingCurrentValue.visibility = View.GONE
                textViewStartingCurrentValue.text = ""
            }
            if (battery!!.productCode.isNotEmpty()) {
                textViewProductCodeTitle.visibility = View.VISIBLE
                textViewProductCodeValue.visibility = View.VISIBLE
                textViewProductCodeValue.text = battery!!.productCode
            } else {
                textViewProductCodeTitle.visibility = View.GONE
                textViewProductCodeValue.visibility = View.GONE
                textViewProductCodeValue.text = ""
            }
        }
        return view
    }


}
