package ru.avtomativ.service.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.style.RelativeSizeSpan
import android.view.View
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_car_battery_details.*
import ru.avtomativ.service.R
import ru.avtomativ.service.adapter.BatteryDetailAdapter
import ru.avtomativ.service.global_var.GlobalVariable
import ru.avtomativ.service.model.CarBatteryDetailsModel
import ru.avtomativ.service.presenter.CarBatteryDetailsPresenter
import ru.avtomativ.service.view.CarBatteryDetailsView

class CarBatteryDetailsActivity : AppCompatActivity(), CarBatteryDetailsView {
    private lateinit var carBatteryDetails: CarBatteryDetailsModel
    private lateinit var id: String
    private lateinit var title: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState != null) {
            if (savedInstanceState.getString(GlobalVariable.CURRENT_CITY) != null) {
                GlobalVariable.currCity = savedInstanceState.getString(GlobalVariable.CURRENT_CITY)!!
            }
            if (savedInstanceState.getString(GlobalVariable.CURRENT_CITY_VIEW) != null) {
                GlobalVariable.currCityView = savedInstanceState.getString(GlobalVariable.CURRENT_CITY_VIEW)!!
            }
            if (savedInstanceState.getString(GlobalVariable.ID) != null) {
                id = savedInstanceState.getString(GlobalVariable.ID)!!
            }
            if (savedInstanceState.getString(GlobalVariable.TITLE) != null) {
                title = savedInstanceState.getString(GlobalVariable.TITLE)!!
            }
        }

        if (intent.extras != null) {
            if (intent.getStringExtra(GlobalVariable.ID) != null) {
                id = intent.getStringExtra(GlobalVariable.ID)
            }
            if (intent.getStringExtra(GlobalVariable.TITLE) != null) {
                title = intent.getStringExtra(GlobalVariable.TITLE)
            }
        }

        supportActionBar?.title = title

        setContentView(R.layout.activity_car_battery_details)
        progressBarDetailsActivity.visibility = View.VISIBLE

        // Вызов метода презентера для получения подробного описания АКБ от API сервиса
        CarBatteryDetailsPresenter(this).getCarBatteryDetales(GlobalVariable.currCity, id)
        // Реализация листенера клика на кнопку Повтор
        buttonRetryDetail.setOnClickListener(this::onClickRetry)
    }

    // Метод обрабатывающий ошибки вернувшиеся после запроса к API сервису
    override fun returnCarBatteryDetails(carBatteryDetails: CarBatteryDetailsModel) {
        this.carBatteryDetails = carBatteryDetails
        progressBarDetailsActivity.visibility = View.GONE
        textViewBatteryTitleInDetails.visibility = View.VISIBLE
        textViewBatteryTitleInDetails.text = carBatteryDetails.title
        if (carBatteryDetails.subtitle.isNotEmpty()) {
            textViewSubtitle.visibility = View.GONE
            val title: String = "%s %s".format(carBatteryDetails.title, carBatteryDetails.subtitle)
            val span: Spannable = SpannableString(title)
            span.setSpan(
                RelativeSizeSpan(0.7f),
                carBatteryDetails.title.length,
                title.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            textViewBatteryTitleInDetails.text = span
        } else {
            textViewSubtitle.visibility = View.GONE
            textViewSubtitle.text = ""
        }
        if (carBatteryDetails.image.isNotEmpty()) {
            imageViewBatteryInDetails.visibility = View.GONE
            Picasso.with(this).load(carBatteryDetails.image).into(imageViewBatteryInDetails)
        } else {
            imageViewBatteryInDetails.visibility = View.GONE
        }
        if (carBatteryDetails.exchangePrice.isNotEmpty()) {
            textViewExchangePrice.visibility = View.VISIBLE
            textViewConditionInDetails.visibility = View.VISIBLE
            textViewExchangePrice.text = carBatteryDetails.exchangePrice
        } else {
            textViewExchangePrice.visibility = View.GONE
            textViewConditionInDetails.visibility = View.GONE
            textViewExchangePrice.text = ""
        }
        if (carBatteryDetails.price.isNotEmpty()) {
            textViewPriceInDetails.visibility = View.VISIBLE
            textViewPriceInDetails.text = carBatteryDetails.price
        } else {
            textViewPriceInDetails.visibility = View.GONE
            textViewPriceInDetails.text = ""
        }
        viewPagerDetails.visibility = View.VISIBLE
        tabLayoutDetails.visibility = View.VISIBLE

        val batteryDetailAdapter: BatteryDetailAdapter = BatteryDetailAdapter(supportFragmentManager, this)
        WhereToBuyFragment.shops = carBatteryDetails.shop
        batteryDetailAdapter.addFragment(WhereToBuyFragment(), getString(R.string.where_to_buy))
        ProductDescriptionFragment.battery = carBatteryDetails
        batteryDetailAdapter.addFragment(ProductDescriptionFragment(), getString(R.string.product_description))
        viewPagerDetails.adapter = batteryDetailAdapter

        tabLayoutDetails.setupWithViewPager(viewPagerDetails)
    }

    // Метод обрабатывающий данные вернувшиеся после запроса к API сервису
    override fun returnCarBatteryDetailsError(error: String) {
        progressBarDetailsActivity.visibility = View.GONE // Скрытие прогресс бара
        // Отображаем сообщение об ошибке
        constraintLayoutErrorConnectionDetail.visibility = View.VISIBLE
    }

    // Метод реализующий листенер клика на кнопку Повтор
    private fun onClickRetry(view: View) {
        // Скрытие сообщения об ошибке
        constraintLayoutErrorConnectionDetail.visibility = View.GONE
        // Отображение прогресс бара
        progressBarDetailsActivity.visibility = View.VISIBLE
        // Вызов метода презентера для получения подробного описания АКБ от API сервиса
        CarBatteryDetailsPresenter(this).getCarBatteryDetales(
            GlobalVariable.currCity,
            intent.extras!!.getString("ID")!!
        )
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putString(GlobalVariable.CURRENT_CITY, GlobalVariable.currCity)
            putString(GlobalVariable.CURRENT_CITY_VIEW, GlobalVariable.currCityView)
            putString(GlobalVariable.ID, id)
            putString(GlobalVariable.TITLE, title)
        }
        super.onSaveInstanceState(outState)
    }
}
