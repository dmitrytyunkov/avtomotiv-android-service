package ru.avtomativ.service.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import ru.avtomativ.service.R
import ru.avtomativ.service.global_var.GlobalVariable

class MainMenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState != null) {
            if (savedInstanceState.getString(GlobalVariable.CURRENT_CITY) != null) {
                GlobalVariable.currCity = savedInstanceState.getString(GlobalVariable.CURRENT_CITY)!!
            }
            if (savedInstanceState.getString(GlobalVariable.CURRENT_CITY_VIEW) != null) {
                GlobalVariable.currCityView = savedInstanceState.getString(GlobalVariable.CURRENT_CITY_VIEW)!!
            }
        }

        supportActionBar?.hide() // Скрытие тулбара
        // Отображение логотипа
        Picasso.with(this).load(R.drawable.logo_main_png).into(imageViewMainMenuLogo)
        // Реализация листенера клика на кнопку Подбор аккумулятора
        buttonBatterySelection.setOnClickListener(this::onClickBatterySelection)
        // Реализация листенера клика на кнопку Каталог аккумуляторов
        buttonCatalogBattery.setOnClickListener(this::onClickCatalogBattery)
        // Реализация листенера клика на кнопку А-Сервис
        buttonAService.setOnClickListener(this::onClickAService)
        // Реализация листенера клика на кнопку Магазины
        buttonShops.setOnClickListener(this::onClickShops)
    }

    // Метод реализующий листенер клика на кнопку Подбор аккумулятора
    private fun onClickBatterySelection(view: View) {
        Toast.makeText(this, "Функционал по подбору аккамулятора пока не реализован", Toast.LENGTH_LONG).show()
    }

    // Метод реализующий листенер клика на кнопку Каталог аккумуляторов
    private fun onClickCatalogBattery(view: View) {
        val intent = Intent(this, MainCatalogActivity::class.java)
        startActivity(intent)
    }

    // Метод реализующий листенер клика на кнопку А-Сервис
    private fun onClickAService(view: View) {
        Toast.makeText(this, "Функционал по отображению информации об А-Сервисе пока не реализован", Toast.LENGTH_LONG)
            .show()
    }

    // Метод реализующий листенер клика на кнопку Магазины
    private fun onClickShops(view: View) {
        Toast.makeText(this, "Функционал по отображению магазинов пока не реализован", Toast.LENGTH_LONG).show()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putString(GlobalVariable.CURRENT_CITY, GlobalVariable.currCity)
            putString(GlobalVariable.CURRENT_CITY_VIEW, GlobalVariable.currCityView)
        }
        super.onSaveInstanceState(outState)
    }
}
