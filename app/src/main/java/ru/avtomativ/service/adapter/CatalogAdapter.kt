package ru.avtomativ.service.adapter

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import ru.avtomativ.service.R
import ru.avtomativ.service.global_var.GlobalVariable
import ru.avtomativ.service.model.CarBatteryCatalogModel
import ru.avtomativ.service.ui.CarBatteryDetailsActivity

class CatalogAdapter : RecyclerView.Adapter<CatalogAdapter.CatalogViewHolder>() {
    private var catalogItems: MutableList<CarBatteryCatalogModel> = emptyList<CarBatteryCatalogModel>().toMutableList()
    // Метод для добавления новых элементов в отображение
    fun setCatalogItems(catalogItems: List<CarBatteryCatalogModel>) {
        this.catalogItems.addAll(catalogItems)
        notifyDataSetChanged()
    }

    // Очистка всех элементов
    fun clearCatalogItems() {
        this.catalogItems.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CatalogViewHolder {
        val view: View = LayoutInflater.from(p0.context).inflate(R.layout.view_catalog_item, p0, false)
        return CatalogViewHolder(view)
    }

    override fun getItemCount(): Int {
        return catalogItems.size
    }

    override fun onBindViewHolder(p0: CatalogViewHolder, p1: Int) {
        p0.bind(catalogItems[p1])
        // Создание листенера для обработки клика на элемент каталога
        p0.itemView.setOnClickListener {
            val intent = Intent(p0.itemView.context, CarBatteryDetailsActivity::class.java)
            intent.putExtra(GlobalVariable.ID, catalogItems[p1].id)
            intent.putExtra(GlobalVariable.TITLE, catalogItems[p1].title)
            p0.itemView.context.startActivity(intent)
        }
    }

    class CatalogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageViewBattery: ImageView = itemView.findViewById(R.id.imageViewBattery)
        var textViewBatteryTitle: TextView = itemView.findViewById(R.id.textViewBatteryTitle)
        var textViewCapacityTitle: TextView = itemView.findViewById(R.id.textViewCharacteristic1Title)
        var textViewCapacityValue: TextView = itemView.findViewById(R.id.textViewCharacteristic1Value)
        var textViewStartingCurrentTitle: TextView = itemView.findViewById(R.id.textViewCharacteristic2Title)
        var textViewStartingCurrentValue: TextView = itemView.findViewById(R.id.textViewCharacteristic2Value)
        var textViewPolarityTitle: TextView = itemView.findViewById(R.id.textViewCharacteristic3Title)
        var textViewPolarityValue: TextView = itemView.findViewById(R.id.textViewCharacteristic3Value)
        var textViewDimensionsTitle: TextView = itemView.findViewById(R.id.textViewCharacteristic4Title)
        var textViewDimensionsValue: TextView = itemView.findViewById(R.id.textViewCharacteristic4Value)
        var textViewWarrantyTitle: TextView = itemView.findViewById(R.id.textViewCharacteristic5Title)
        var textViewWarrantyValue: TextView = itemView.findViewById(R.id.textViewCharacteristic5Value)
        var textViewPrice: TextView = itemView.findViewById(R.id.textViewPrice)
        var textViewCondition: TextView = itemView.findViewById(R.id.textViewCondition)
        var imageViewRecommend: ImageView = itemView.findViewById(R.id.imageViewRecommend)
        var imageViewSale: ImageView = itemView.findViewById(R.id.imageViewSale)

        fun bind(catalogItem: CarBatteryCatalogModel) {
            Picasso.with(itemView.context).load(catalogItem.image).into(imageViewBattery)

            textViewBatteryTitle.text = catalogItem.title

            if (catalogItem.recommend) {
                imageViewRecommend.visibility = View.VISIBLE
                Picasso.with(itemView.context).load(R.drawable.img_recommend).into(imageViewRecommend)
            } else {
                imageViewRecommend.visibility = View.GONE
            }
            if (catalogItem.goodsSale) {
                imageViewSale.visibility = View.VISIBLE
                Picasso.with(itemView.context).load(R.drawable.img_sale).into(imageViewSale)
            } else {
                imageViewSale.visibility = View.GONE
            }
            if (catalogItem.capacity.isNotEmpty()) {
                textViewCapacityTitle.visibility = View.VISIBLE
                textViewCapacityValue.visibility = View.VISIBLE
                textViewCapacityValue.text = catalogItem.capacity
            } else {
                textViewCapacityTitle.visibility = View.GONE
                textViewCapacityValue.visibility = View.GONE
                textViewCapacityValue.text = ""
            }
            if (catalogItem.startingCurrent.isNotEmpty()) {
                textViewStartingCurrentTitle.visibility = View.VISIBLE
                textViewStartingCurrentValue.visibility = View.VISIBLE
                textViewStartingCurrentValue.text = catalogItem.startingCurrent
            } else {
                textViewStartingCurrentTitle.visibility = View.GONE
                textViewStartingCurrentValue.visibility = View.GONE
                textViewStartingCurrentValue.text = ""
            }
            if (catalogItem.polarity.isNotEmpty()) {
                textViewPolarityTitle.visibility = View.VISIBLE
                textViewPolarityValue.visibility = View.VISIBLE
                textViewPolarityValue.text = catalogItem.polarity
            } else {
                textViewPolarityTitle.visibility = View.GONE
                textViewPolarityValue.visibility = View.GONE
                textViewPolarityValue.text = ""
            }
            if (catalogItem.dimensions.isNotEmpty()) {
                textViewDimensionsTitle.visibility = View.VISIBLE
                textViewDimensionsValue.visibility = View.VISIBLE
                textViewDimensionsValue.text = catalogItem.dimensions
            } else {
                textViewDimensionsTitle.visibility = View.GONE
                textViewDimensionsValue.visibility = View.GONE
                textViewDimensionsValue.text = ""
            }
            if (catalogItem.warranty.isNotEmpty()) {
                textViewWarrantyTitle.visibility = View.VISIBLE
                textViewWarrantyValue.visibility = View.VISIBLE
                textViewWarrantyValue.text = catalogItem.warranty
            } else {
                textViewWarrantyTitle.visibility = View.GONE
                textViewWarrantyValue.visibility = View.GONE
                textViewWarrantyValue.text = ""
            }
            if (catalogItem.exchangePrice.isNotEmpty()) {
                textViewPrice.visibility = View.VISIBLE
                textViewCondition.visibility = View.VISIBLE
                textViewPrice.text = catalogItem.exchangePrice
            } else {
                textViewPrice.visibility = View.GONE
                textViewCondition.visibility = View.GONE
                textViewPrice.text = ""
            }
        }
    }
}