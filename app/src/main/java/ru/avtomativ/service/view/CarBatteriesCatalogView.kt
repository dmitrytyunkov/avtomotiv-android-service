package ru.avtomativ.service.view

import ru.avtomativ.service.model.CarBatteryCatalogModel

interface CarBatteriesCatalogView {
    // Метод обрабатывающий данные вернувшиеся после запроса к API сервису
    fun returnCarBatteriesCatalog(carBatteriesCatalog: List<CarBatteryCatalogModel>)

    // Метод обрабатывающий ошибки вернувшиеся после запроса к API сервису
    fun returnCarBatteriesCatalogError(error: String)
}