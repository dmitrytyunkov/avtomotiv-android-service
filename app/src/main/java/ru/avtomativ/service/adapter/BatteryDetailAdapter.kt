package ru.avtomativ.service.adapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class BatteryDetailAdapter(fm: FragmentManager, private val context: Context) : FragmentPagerAdapter(fm) {
    private val fragments: MutableList<Fragment> = emptyList<Fragment>().toMutableList()
    private val titls: MutableList<String> = emptyList<String>().toMutableList()

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getItem(p0: Int): Fragment {
        return fragments[p0]
    }

    override fun getPageTitle(p0: Int): CharSequence? {
        return titls[p0]
    }

    fun addFragment(fragment: Fragment, title: String) {
        fragments.add(fragment)
        titls.add(title)
    }
}