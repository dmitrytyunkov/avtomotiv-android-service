package ru.avtomativ.service.presenter

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.avtomativ.service.global_var.GlobalVariable
import ru.avtomativ.service.model.CarBatteryDetailsModel
import ru.avtomativ.service.network.AvtomotivService
import ru.avtomativ.service.view.CarBatteryDetailsView

class CarBatteryDetailsPresenter(private val carBatteryDetailsView: CarBatteryDetailsView) {
    fun getCarBatteryDetales(city: String, id: String) {
        val avtomotivService = AvtomotivService.create(GlobalVariable.BASE_URL) // Создаем подключение к API сервису
        avtomotivService.getCarBatteryDetail(city, id).enqueue(object : Callback<CarBatteryDetailsModel> {
            override fun onFailure(call: Call<CarBatteryDetailsModel>, t: Throwable) {
                Log.d("REQUEST ERROR", t.message)
                carBatteryDetailsView.returnCarBatteryDetailsError(t.message!!)
            }

            override fun onResponse(
                call: Call<CarBatteryDetailsModel>,
                response: Response<CarBatteryDetailsModel>
            ) {
                if (response.code() != 200) {
                    Log.d("REQUEST ERROR", response.code().toString() + ' '.toString() + response.message())
                    carBatteryDetailsView.returnCarBatteryDetailsError(response.code().toString() + ' '.toString() + response.message())
                } else {
                    Log.d("REQUEST OK", response.body()!!.toString())
                    carBatteryDetailsView.returnCarBatteryDetails(response.body()!!)
                }
            }
        })
    }
}