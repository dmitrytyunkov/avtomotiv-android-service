package ru.avtomativ.service.view

import ru.avtomativ.service.model.CityModel

interface SelectCityView {
    // Метод обрабатывающий данные вернувшиеся после запроса к API сервису
    fun returnCities(cities: List<CityModel>)

    // Метод обрабатывающий ошибки вернувшиеся после запроса к API сервису
    fun returnCitiesError(error: String)
}