package ru.avtomativ.service.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main_catalog.*
import ru.avtomativ.service.R
import ru.avtomativ.service.global_var.GlobalVariable

class MainCatalogActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_catalog)

        if (savedInstanceState != null) {
            if (savedInstanceState.getString(GlobalVariable.CURRENT_CITY) != null) {
                GlobalVariable.currCity = savedInstanceState.getString(GlobalVariable.CURRENT_CITY)!!
            }
            if (savedInstanceState.getString(GlobalVariable.CURRENT_CITY_VIEW) != null) {
                GlobalVariable.currCityView = savedInstanceState.getString(GlobalVariable.CURRENT_CITY_VIEW)!!
            }
        }

        // Реализация листенера клика на кнопку Автомобильные аккумуляторы
        buttonCarBatteries.setOnClickListener(this::onClickCarBatteries)
        // Реализация листенера клика на кнопку Грузовые аккумуляторы
        buttonFreightBatteries.setOnClickListener(this::onClickFreightBatteries)
        // Реализация листенера клика на кнопку Мото аккумуляторы
        buttonMotoBatteries.setOnClickListener(this::onClickMotoBatteries)
        // Реализация листенера клика на кнопку Товары для аккумуляторов
        buttonProductsForBatteries.setOnClickListener(this::onClickProductsForBatteries)
        // Реализация листенера клика на кнопку Катерные аккумуляторы
        buttonCutterBatteries.setOnClickListener(this::onClickCutterBatteries)
        // Реализация листенера клика на кнопку Стационарные аккумуляторы
        buttonStationaryBatteries.setOnClickListener(this::onClickStationaryBatteries)
    }

    // Метод реализующий листенер клика на кнопку Автомобильные аккумуляторы
    private fun onClickCarBatteries(view: View) {
        val intent = Intent(this, CarBatteriesCatalogActivity::class.java)
        startActivity(intent)
    }

    // Метод реализующий листенер клика на кнопку Грузовые аккумуляторы
    private fun onClickFreightBatteries(view: View) {
        Toast.makeText(this, "Функционал католога грузовых аккумуляторов пока не реализован", Toast.LENGTH_LONG).show()
    }

    // Метод реализующий листенер клика на кнопку Мото аккумуляторы
    private fun onClickMotoBatteries(view: View) {
        Toast.makeText(this, "Функционал католога мото аккумуляторов пока не реализован", Toast.LENGTH_LONG).show()
    }

    // Метод реализующий листенер клика на кнопку Товары для аккумуляторов
    private fun onClickProductsForBatteries(view: View) {
        Toast.makeText(this, "Функционал католога товаров для аккумуляторов пока не реализован", Toast.LENGTH_LONG)
            .show()
    }

    // Метод реализующий листенер клика на кнопку Катерные аккумуляторы
    private fun onClickCutterBatteries(view: View) {
        Toast.makeText(this, "Функционал католога катерных аккумуляторов пока не реализован", Toast.LENGTH_LONG).show()
    }

    // Метод реализующий листенер клика на кнопку Стационарные аккумуляторы
    private fun onClickStationaryBatteries(view: View) {
        Toast.makeText(this, "Функционал католога промышленных аккумуляторов пока не реализован", Toast.LENGTH_LONG)
            .show()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putString(GlobalVariable.CURRENT_CITY, GlobalVariable.currCity)
            putString(GlobalVariable.CURRENT_CITY_VIEW, GlobalVariable.currCityView)
        }
        super.onSaveInstanceState(outState)
    }
}
