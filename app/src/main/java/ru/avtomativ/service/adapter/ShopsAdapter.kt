package ru.avtomativ.service.adapter

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.avtomativ.service.R
import ru.avtomativ.service.model.Shop

class ShopsAdapter : RecyclerView.Adapter<ShopsAdapter.CatalogViewHolder>() {
    private var shops: MutableList<Shop> = emptyList<Shop>().toMutableList()
    // Метод для добавления новых элементов в отображение
    fun setCatalogItems(catalogItems: List<Shop>) {
        this.shops.addAll(catalogItems)
        notifyDataSetChanged()
    }

    // Очистка всех элементов
    fun clearCatalogItems() {
        this.shops.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CatalogViewHolder {
        val view: View = LayoutInflater.from(p0.context).inflate(R.layout.view_shop_item, p0, false)
        return CatalogViewHolder(view)
    }

    override fun getItemCount(): Int {
        return shops.size
    }

    override fun onBindViewHolder(p0: CatalogViewHolder, p1: Int) {
        p0.bind(shops[p1])
    }

    class CatalogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var textViewShopAddress: TextView = itemView.findViewById(R.id.textViewShopAddress)
        var textViewShopPhone: TextView = itemView.findViewById(R.id.textViewShopPhone)
        var textViewShopStatus: TextView = itemView.findViewById(R.id.textViewShopStatus)
        var constraintLayoutShopStatus: ConstraintLayout = itemView.findViewById(R.id.constraintLayoutShopStatus)

        fun bind(shop: Shop) {
            textViewShopAddress.text = shop.shopAddress
            textViewShopPhone.text = shop.shopPhone
            textViewShopStatus.text = shop.shopStatus
            if (shop.shopStatus == "есть в наличии") {
                constraintLayoutShopStatus.setBackgroundResource(R.drawable.shop_item_in_stock)
                textViewShopStatus.setTextColor(itemView.resources.getColor(R.color.colorShopBorderInStock))
            } else if (shop.shopStatus == "уточняйте у менеджера") {
                constraintLayoutShopStatus.setBackgroundResource(R.drawable.shop_item_specify)
                textViewShopStatus.setTextColor(itemView.resources.getColor(R.color.colorShopBorderSpecify))
            }
        }
    }
}