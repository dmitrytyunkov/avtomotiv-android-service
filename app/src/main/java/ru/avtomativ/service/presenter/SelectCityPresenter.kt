package ru.avtomativ.service.presenter

import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.avtomativ.service.global_var.GlobalVariable
import ru.avtomativ.service.model.CityModel
import ru.avtomativ.service.network.AvtomotivService
import ru.avtomativ.service.view.SelectCityView

class SelectCityPresenter(private val selectCityView: SelectCityView) {
    fun getCities() {
        val avtomotivService = AvtomotivService.create(GlobalVariable.BASE_URL) // Создаем подключение к API сервису
        avtomotivService.getCities().enqueue(object : Callback<List<CityModel>> {
            override fun onFailure(call: Call<List<CityModel>>, t: Throwable) {
                Log.d("REQUEST ERROR", t.message)
                selectCityView.returnCitiesError(t.message!!)
            }

            override fun onResponse(call: Call<List<CityModel>>, response: Response<List<CityModel>>) {
                if (response.code() != 200) {
                    Log.d("REQUEST ERROR", response.code().toString() + ' '.toString() + response.message())
                    selectCityView.returnCitiesError(response.code().toString() + ' '.toString() + response.message())
                } else {
                    Log.d("REQUEST OK", response.body()!!.toString())
                    selectCityView.returnCities(response.body()!!)
                }
            }
        })
    }
}