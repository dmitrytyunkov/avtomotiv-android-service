package ru.avtomativ.service.adapter

import android.widget.ArrayAdapter
import ru.avtomativ.service.R
import ru.avtomativ.service.model.CityModel
import ru.avtomativ.service.ui.SelectCityActivity

class CitiesAdapter {
    companion object {
        // Создание адаптера для выпадающего списка городов
        fun getAdapter(context: SelectCityActivity, cities: List<CityModel>): ArrayAdapter<CityModel> {
            val arrayAdapter = ArrayAdapter(context, R.layout.view_spinner_item, cities)
            arrayAdapter.setDropDownViewResource(R.layout.view_spinner_dropdown_item)
            return arrayAdapter
        }
    }
}