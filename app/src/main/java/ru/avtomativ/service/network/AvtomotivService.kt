package ru.avtomativ.service.network

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import ru.avtomativ.service.model.CarBatteryCatalogModel
import ru.avtomativ.service.model.CarBatteryDetailsModel
import ru.avtomativ.service.model.CityModel

interface AvtomotivService {
    @GET("api/v1/get-cities")
    fun getCities(): Call<List<CityModel>>

    @GET("api/v1/car-batteries-catalog")
    fun getCarBatteriesCatalog(
        @Query("city") city: String,
        @Query("p") page: Int,
        @Query("from-capacity") fromCapacity: Int,
        @Query("to-capacity") toCapacity: Int,
        @Query("from-starting-current") fromStartingCurrent: Int,
        @Query("to-starting-current") toStartingCurrent: Int
    ): Call<List<CarBatteryCatalogModel>>

    @GET("api/v1/car-battery")
    fun getCarBatteryDetail(
        @Query("city") city: String,
        @Query("id") id: String
    ): Call<CarBatteryDetailsModel>

    companion object Factory {
        fun create(url: String): AvtomotivService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(url)
                .client(UnsafeOkHttpClient.getUnsafeOkHttpClient())
                .build()

            return retrofit.create(AvtomotivService::class.java)
        }
    }
}