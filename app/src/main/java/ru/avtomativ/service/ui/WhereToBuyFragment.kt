package ru.avtomativ.service.ui


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.avtomativ.service.R
import ru.avtomativ.service.adapter.ShopsAdapter
import ru.avtomativ.service.model.Shop

private const val ARG_SHOP = "shops"

/**
 * A simple [Fragment] subclass.
 *
 */
open class WhereToBuyFragment : Fragment() {
    companion object {
        var shops: List<Shop> = emptyList()
    }

    private val shopsAdapter: ShopsAdapter = ShopsAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_where_to_buy, container, false)
        val recyclerViewShops: RecyclerView = view.findViewById(R.id.recyclerViewInStock)
        recyclerViewShops.layoutManager = LinearLayoutManager(context)
        recyclerViewShops.adapter = shopsAdapter
        var shopsWithStatusInStock: MutableList<Shop> = emptyList<Shop>().toMutableList()
        var shopsWithStatusSpecify: MutableList<Shop> = emptyList<Shop>().toMutableList()
        shops.forEach {
            if (it.shopStatus == "есть в наличии") {
                shopsWithStatusInStock.add(it)
            } else if (it.shopStatus == "уточняйте у менеджера") {
                shopsWithStatusSpecify.add(it)
            }
        }
        shopsAdapter.setCatalogItems(shopsWithStatusInStock)
        shopsAdapter.setCatalogItems(shopsWithStatusSpecify)
        return view
    }


}
