package ru.avtomativ.service.model


import com.google.gson.annotations.SerializedName

data class CarBatteryDetailsModel(
    @SerializedName("ag")
    val ag: String,
    @SerializedName("agm")
    val agm: String,
    @SerializedName("bottom_mount")
    val bottomMount: String,
    @SerializedName("brand")
    val brand: String,
    @SerializedName("capacity")
    val capacity: String,
    @SerializedName("class_battery")
    val classBattery: String,
    @SerializedName("efb")
    val efb: String,
    @SerializedName("exchange_price")
    val exchangePrice: String,
    @SerializedName("height")
    val height: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("length")
    val length: String,
    @SerializedName("manufacturer")
    val manufacturer: String,
    @SerializedName("polarity")
    val polarity: String,
    @SerializedName("price")
    val price: String,
    @SerializedName("product_code")
    val productCode: String,
    @SerializedName("production_region")
    val productionRegion: String,
    @SerializedName("punch_frame")
    val punchFrame: String,
    @SerializedName("serviceability")
    val serviceability: String,
    @SerializedName("shop")
    val shop: List<Shop>,
    @SerializedName("standard_size")
    val standardSize: String,
    @SerializedName("starting_current")
    val startingCurrent: String,
    @SerializedName("status")
    val status: String,
    @SerializedName("subtitle")
    val subtitle: String,
    @SerializedName("terminal_type")
    val terminalType: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("voltage")
    val voltage: String,
    @SerializedName("warranty")
    val warranty: String,
    @SerializedName("width")
    val width: String
)